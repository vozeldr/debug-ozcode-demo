﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Simplify
{
    public class SimplifyDemo
    {
        public SimplifyBackend Custom { get; private set; }

        public SimplifyBackend Default { get; private set; }

        public SimplifyDemo()
        {
            Custom = SimplifyBackend.GetCustomBackend();
            Default = SimplifyBackend.GetDefaultBackend();
        }

        public bool PerformCheck()
        {
            return (Custom.IsEnabled && Default.IsEnabled) || (Custom.Count > 0 && Default.Count > 0) && Custom.Name.Equals("Custom") && Default.Name.Equals("Default");
        }
    }
}
