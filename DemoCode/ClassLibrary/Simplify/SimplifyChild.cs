﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Simplify
{
    public class SimplifyChild
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
