﻿using System;
using System.Linq;

namespace Net.Vozel.DebugDemo.ClassLibrary.Simplify
{
    public class SimplifyBackend
    {
        public bool IsEnabled { get; set; }
        public int Count { get; set; }

        public string Name { get; set; }

        public SimplifyChild Child { get; set; }

        private SimplifyBackend()
        {
            
        }

        public static SimplifyBackend GetDefaultBackend()
        {
            return new SimplifyBackend
            {
                IsEnabled = false,
                Count = 10,
                Name = "Default",
                Child = new SimplifyChild { Name = "Default", Value = 5 }
            };
        }

        public static SimplifyBackend GetCustomBackend()
        {
            return new SimplifyBackend
            {
                IsEnabled = true,
                Count = 4,
                Name = "Custom",
                Child = new SimplifyChild { Name = "Custom", Value = 10 }
            };
        }
    }
}
