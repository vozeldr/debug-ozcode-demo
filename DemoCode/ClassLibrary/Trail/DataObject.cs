﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Trail
{
    public class DataObject
    {
        public string Details { get; set; }
        public int Value { get; set; }

        public DataObject()
        {

        }

        public DataObject(string details)
        {
            Details = details;
        }
    }
}
