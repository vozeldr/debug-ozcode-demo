﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Trail
{
    public class CustomTwoException : Exception
    {
        public CustomTwoException(string message, Exception innerException) : base(message, innerException) { }
    }
}
