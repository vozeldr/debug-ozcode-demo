﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Trail
{
    public class CustomThreeException : Exception
    {
        public string CustomString { get; set; }
        public int CustomInt { get; set; }

        public CustomThreeException(string message, Exception innerException) : base(message, innerException) { }
    }
}
