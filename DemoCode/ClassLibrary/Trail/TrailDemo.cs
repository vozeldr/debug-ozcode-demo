﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Trail
{
    public class TrailDemo
    {
        public static string MyProperty { get; set; }

        public static void PerformDemo()
        {
            MyProperty = "Hi";
            TrailDemo demo = new TrailDemo();
            demo.FirstMethod(new DataObject("Woohoo"));
            demo.FirstMethod(new DataObject());
        }

        public string FirstMethod(DataObject obj)
        {
            try
            {
                return SecondMethod(obj.Details);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public string SecondMethod(string value)
        {
            try
            {
                return ThirdMethod(value);
            }
            catch (Exception ex)
            {
                throw new CustomOneException("Error in second method.", ex);
            }
        }

        public string ThirdMethod(string value)
        {
            try
            {
                return FourthMethod(value);
            }
            catch (Exception ex)
            {
                throw new CustomTwoException("Error in third method.", ex);
            }
        }

        public string FourthMethod(string value)
        {
            try
            {
                return value.Reverse().ToString();
            }
            catch (NullReferenceException ex)
            {
                throw new CustomThreeException("Value was null in fourth method.", ex);
            }
        }
    }
}
