﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Search
{
    public class SearchDemo
    {
        public List<Contact> Contacts { get; set; }

        public SearchDemo()
        {
            ContactService service = new ContactService();
            Contacts = service.GetContacts();
        }

        public bool PerformDemo()
        {
            foreach (Contact c in Contacts)
            {
                DoSomething(c);
            }
            return true;
        }

        private void DoSomething(Contact c)
        {
            int streetType = Int32.Parse(c.Addresses[0].Street.Split(' ')[0]) % 2;

            switch (streetType)
            {
                case 0:
                    Console.WriteLine("Street type is a street");
                    break;
                case 1:
                    Console.WriteLine("Street type is an avenue");
                    c.Addresses[0].City = "Somewhere";
                    break;
                default:
                    Console.WriteLine("Street type is unknown");
                    break;
            }

            return;
        }

        
    }
}