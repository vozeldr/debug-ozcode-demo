﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.Search
{
    public class Contact
    {
        public string Name { get; set; }

        public List<string> PhoneNumbers { get; set; }

        public List<Address> Addresses { get; set; }

        public Contact(string name, string number, string street, string city, string state, string zip)
        {
            Name = name;
            PhoneNumbers = new List<string>();
            PhoneNumbers.Add(number);
            Addresses = new List<Address>();
            Addresses.Add(new Address { Street = street, City = city, State = state, Zip = zip });
        }
    }
}
