﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.ConditionalCompilation
{
    public class ConditionalDemo
    {
        public bool SomeValue { get; set; }

        public ConditionalDemo()
        {
#if DEBUG
            SomeValue = false;
#else
            SomeValue = true;
#endif
        }

        public bool PerformDemo()
        {
            return SomeValue;
        }
    }
}
