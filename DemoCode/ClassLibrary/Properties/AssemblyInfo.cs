﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;

[assembly: AssemblyTitle("Debugging Demo Library")]
[assembly: AssemblyDescription("Class library for the debugging demo.")]
[assembly: Guid("72498a27-7f39-43b4-aece-4172677ca3e3")]
[assembly: XmlnsDefinition("drv-debug-demo", "Net.Vozel.DebugDemo")]
[assembly: InternalsVisibleTo("Net.Vozel.DebugDemo.UnitTest", AllInternalsVisible=true)]