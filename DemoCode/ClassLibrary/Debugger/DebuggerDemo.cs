﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Net.Vozel.DebugDemo.ClassLibrary.BasicDebug;
using DB = System.Diagnostics.Debugger;

namespace Net.Vozel.DebugDemo.ClassLibrary.Debugger
{
    public class DebuggerDemo
    {
        public static bool PerformDemo()
        {
            DB.Break();

            bool result = BasicDemo.FigureItOut("Boomsticks");

            if (DB.IsAttached && DB.IsLogging())
            {
                DB.Log(5, "MINE", String.Format("The result of the method was: {0}.", result));
            }

            if (!DB.IsAttached)
            {
                DB.Launch();
            }

            return result;
        }
    }
}
