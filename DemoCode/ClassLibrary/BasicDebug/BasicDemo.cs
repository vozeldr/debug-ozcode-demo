﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Vozel.DebugDemo.ClassLibrary.BasicDebug
{
    public class BasicDemo
    {
        public static bool FigureItOut(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            int value = 0;
            IEnumerable<char> reverse = input.Reverse();
            foreach (char c in reverse)
            {
                value += GetValueForChar(c);
            }
            return value > 10;
        }

        private static int GetValueForChar(char c) {
            switch (c)
            {
                case 'A':
                    return 1;
                case 'R':
                    return 2;
                case 'a':
                    return 3;
                default:
                    return 0;
            }
        }
    }
}
