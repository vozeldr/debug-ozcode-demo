
using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Dewey Vozel")]
[assembly: AssemblyProduct("Debugging Presentation")]
[assembly: AssemblyCopyright("Copyright ? Dewey Vozel 2015")]
[assembly: AssemblyTrademark("")]

#if DEBUG
    [assembly: AssemblyConfiguration("Debug")]
#else
    [assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]

[assembly: AssemblyVersion("1.0")]
[assembly: AssemblyFileVersion("1.0.30.1350")]
[assembly: AssemblyInformationalVersion("1.0 Beta")]

