﻿using System.Reflection;
using System.Windows;

[assembly: AssemblyTitle("Debug Demo Desktop Application")]
[assembly: AssemblyDescription("Desktop application for the debugging demo.")]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]