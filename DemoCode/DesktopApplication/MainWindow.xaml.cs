﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Net.Vozel.DebugDemo.ClassLibrary.BasicDebug;
using Net.Vozel.DebugDemo.ClassLibrary.ConditionalCompilation;
using Net.Vozel.DebugDemo.ClassLibrary.Debugger;
using Net.Vozel.DebugDemo.ClassLibrary.Search;
using Net.Vozel.DebugDemo.ClassLibrary.Simplify;
using Net.Vozel.DebugDemo.ClassLibrary.Trail;
using dataObj = Net.Vozel.DebugDemo.ClassLibrary.Trail.DataObject;

namespace Net.Vozel.DebugDemo.DesktopApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            dataObj what = new dataObj("What?");
        }

        public void Button_Click_5(object sender, RoutedEventArgs e)
        {
            TrailDemo.PerformDemo();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SimplifyDemo demo = new SimplifyDemo();
            ((Button)sender).IsEnabled = demo.PerformCheck();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ConditionalDemo demo = new ConditionalDemo();
            ((Button)sender).IsEnabled = demo.PerformDemo();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = BasicDemo.FigureItOut("A long time ago in a galaxy far, far away...");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = DebuggerDemo.PerformDemo();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            SearchDemo demo = new SearchDemo();
            ((Button)sender).IsEnabled = demo.PerformDemo();
        }
    }
}
